@extends('layouts.app')

@section('content')
<div class="container">
<div>
<div>
     <div >
         новое задание
    </div>

        <div>
                   
        @include('common.errors')

                  
            <form action="{{ url('task')}}" method="POST" >
              {{ csrf_field() }}

                       
                   <div >
                     <label for="task-name">добавить</label>

         <div class="col-sm-6">
             <input type="text" name="name" id="task-name" " value="{{ old('task') }}">
         </div>
         </div>

                       
        <div>
         <div>
          <button type="submit" >+ </button>
          </div>
      </div>
                 </form>
                </div>
            </div>

         @if (count($tasks) > 0)
        <div>
          <div>
              доступные задания
          </div>

         <div>
        <table class="table table-striped task-table">
          <thead>
        <th>задания</th>
         <th>&nbsp;</th>
             </thead>
         <tbody>
             @foreach ($tasks as $task)
                 <tr>
                <td class="table-text"><div>{{ $task->name }}</div></td>

                        <td>
                    <form action="{{ url('task/'.$task->id) }}" method="POST">
                  {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                     <button type="submit" class="btn">
                      <i>✔</i>
                    </button>
                    </form>
                    </td>
                 </tr>
                @endforeach
             </tbody>
             </table>
</div>
 </div>
 @endif
 </div>
</div>
@endsection
